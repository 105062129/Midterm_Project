service cloud.firestore {
  match /databases/{database}/documents {

  
    function isSelf(param) {
  	return request.auth.uid == param;
    }
    
    function isUser() {
  	return request.auth != null;
    }
    
    function checkStrAndSize(str, min_neq, max_eq) {
    	return str is string && str.size() > min_neq && str.size() <= max_eq;
    }
    
    function checkTime(param, tolerance_in_s) {
    	return request.time <= param + duration.value(tolerance_in_s, 's');
    }
    
    function checkPublic(uid) {
    	return get(/databases/$(database)/documents/users/$(uid)).data.public == true
    }




  	match /idMapping/{user_id} {
    
    	allow get: if isSelf(request.resource.data.uid) || isUser();
    
    	allow list: if isSelf(request.resource.data.uid) ||
      								isUser() && checkPublic(resource.data.uid);
    	
      // Must be created with UID
      allow create: if isSelf(request.resource.data.uid) && request.resource.data.size() == 1 &&
                        request.resource.data.uid is string &&
                        getAfter(/databases/$(database)/documents/users/$(request.resource.data.uid))
                        .data.id == user_id;
    }
  
  
  
  
    match /users/{user_uid} {
    
      function isInfo() {
      	return request.resource.data.info is map;
      }
      
      function isUserName() {
      	return checkStrAndSize(request.resource.data.userName, 0, 64);
      }
      
      function isId() {
      	return checkStrAndSize(request.resource.data.id, 0, 64) &&
        				request.resource.data.id.matches("[a-zA-Z0-9]+");
      }
           
      function idChanged() {
      	return request.resource.data.id != resource.data.id;
      }
      
      function isPublic() {
      	return request.resource.data.public is bool;
      }
      
      function timeChanged() {
      	return request.resource.data.timestamp != resource.data.timestamp;
      }
      
            
			allow read: if isUser();
      
      // Must be created with Id
      allow create: if isSelf(user_uid) && request.resource.data.size() == 5 &&
      									isInfo() && isUserName() && isId() && isPublic() &&
                				checkTime(request.resource.data.timestamp, 10) &&
                				getAfter(/databases/$(database)/documents/idMapping/$(request.resource.data.id))
              					.data.uid == user_uid;
                        
      allow update: if isSelf(user_uid) && request.resource.data.size() == 5 &&
      									!idChanged() && !timeChanged() && isUserName() && isInfo() && isPublic();
      
      
      
      
      	match /inviteList/{chat_uid} {
        
        	function isInviter() {
          	return request.resource.data.inviter ==
            				get(/databases/$(database)/documents/users/$(request.auth.uid)).data.userName;
          }
        
        	function isInfo() {
          	return request.resource.data.info is map;
          }
        
        
        	allow read: if isSelf(user_uid);
      
      		// Must be created by others who is room's manager
        	allow create: if !isSelf(user_uid) && request.resource.data.size() == 2 &&
          									isInfo() && checkPublic(user_uid) && checkTime(request.resource.data.timestamp, 10) &&
          									!exists(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(user_uid)) &&
          									getAfter(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(request.auth.uid))
              							.data.manager == true;
          
          allow delete: if isSelf(user_uid);
          
        }
        
        match /chatList/{chat_uid} {        	
        
        	allow read: if isSelf(user_uid);
        	
          // Must be created if creating new room or being invited
        	allow create: if isSelf(user_uid) && request.resource.data.size() == 2 &&          									
                            request.resource.data.notification == true &&
                            checkTime(request.resource.data.timestamp, 10) &&
          									(
                            getAfter(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(user_uid))
              							.data.manager == true
                            ||
                            exists(/databases/$(database)/documents/users/$(user_uid)/inviteList/$(chat_uid)) &&
                            getAfter(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(user_uid))
                            .data.size() >= 0
                            );

					// 1. In the room but change the setting of notification
					// 2. Enter the same room again
          allow update: if isSelf(user_uid) && request.resource.data.size() == 2 &&                            
                            (
                            exists(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(user_uid)) &&
                            request.resource.data.notification is bool &&
                            checkTime(request.resource.data.timestamp, 10)
                            ||
                            !exists(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(user_uid)) &&
                            request.resource.data.notification == true &&
                            checkTime(request.resource.data.timestamp, 10) &&
                            exists(/databases/$(database)/documents/users/$(user_uid)/inviteList/$(chat_uid)) &&
                            getAfter(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(user_uid))
                            .data.size() >= 0
                            );
          
        }
    }
    
    
    
    
    match /chatRooms/{chat_uid} {
    
      function isInfo() {
      	return request.resource.data.info is map;
			}
      
      function isRoomMember() {
      	return exists(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(request.auth.uid));
      }
      
      
      allow read: if isUser() &&
      								exists(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(request.auth.uid))
                      ||
                      exists(/databases/$(database)/documents/users/$(request.auth.uid)/inviteList/$(chat_uid));
      
    	allow create: if isUser() && request.resource.data.size() == 2 &&
      									isInfo() &&
      									checkTime(request.resource.data.timestamp, 10) &&
                        getAfter(/databases/$(database)/documents/chatRooms/$(chat_uid)/members/$(request.auth.uid))
              					.data.manager == true &&
                        getAfter(/databases/$(database)/documents/users/$(request.auth.uid)/chatList/$(chat_uid))
                        .data.size() >= 0;
      
      
      
      
      match /members/{member_uid} {
      
      	allow read: if isRoomMember();
      
      	// Must be created if creating new room or being invited
      	allow create: if isSelf(member_uid) && request.resource.data.size() == 1 &&
        									(
        									!exists(/databases/$(database)/documents/chatRooms/$(chat_uid)) &&
                          request.resource.data.manager == true
                          ||
                          exists(/databases/$(database)/documents/users/$(member_uid)/inviteList/$(chat_uid)) &&
                          getAfter(/databases/$(database)/documents/users/$(request.auth.uid)/chatList/$(chat_uid))
                          .data.size() >= 0 &&
                          request.resource.data.manager == false
                          );
                          
      }
      
      match /messages/{msg_id} {
      
      	function isContent() {
        	return request.resource.data.content is map;
        }
      
      	allow read: if isRoomMember();
      
      	allow create: if isSelf(request.resource.data.sender) && request.resource.data.size() == 3 &&
        									isContent() && checkTime(request.resource.data.timestamp, 10) &&
                          isRoomMember();
      
      }
    
    }
    
    
    
  }
}